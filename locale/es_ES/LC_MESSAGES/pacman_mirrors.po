# Translations template for pacman-mirrors.
# Copyright (C) 2017 ORGANIZATION
# This file is distributed under the same license as the pacman-mirrors
# project.
# 
# Translators:
# Xema Fuentes, 2020
msgid ""
msgstr ""
"Project-Id-Version: manjaro-pacman-mirrors\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2017-10-23 20:55+0200\n"
"PO-Revision-Date: 2020-05-31 06:55+0000\n"
"Last-Translator: Xema Fuentes\n"
"Language-Team: Spanish (Spain) (http://www.transifex.com/manjarolinux/manjaro-pacman-mirrors/language/es_ES/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.5.1\n"
"Language: es_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: pacman_mirrors/constants/txt.py:29
msgid "ERROR"
msgstr "ERROR"

#: pacman_mirrors/constants/txt.py:30
msgid "INFO"
msgstr "INFO"

#: pacman_mirrors/constants/txt.py:31
msgid "WARNING"
msgstr "ADVERTENCIA"

#: pacman_mirrors/constants/txt.py:33
msgid "HTTPException"
msgstr "Excepción de HTTP "

#: pacman_mirrors/constants/txt.py:34
msgid "TIMEOUT"
msgstr "TIEMPO DE ESPERA"

#: pacman_mirrors/constants/txt.py:36
msgid "API"
msgstr "API"

#: pacman_mirrors/constants/txt.py:37
msgid "BRANCH"
msgstr "RAMA"

#: pacman_mirrors/constants/txt.py:38
msgid "COUNTRY"
msgstr "PAÍS"

#: pacman_mirrors/constants/txt.py:39
msgid "FILE"
msgstr "ARCHIVO"

#: pacman_mirrors/constants/txt.py:40
msgid "METHOD"
msgstr "MÉTODO"

#: pacman_mirrors/constants/txt.py:41
msgid "METHODS"
msgstr "MÉTODOS"

#: pacman_mirrors/constants/txt.py:42
msgid "MISC"
msgstr "MISCELÁNEA"

#: pacman_mirrors/constants/txt.py:43
msgid "NUMBER"
msgstr "NÚMERO"

#: pacman_mirrors/constants/txt.py:44
msgid "PATH"
msgstr "CAMINO"

#: pacman_mirrors/constants/txt.py:45
msgid "PREFIX"
msgstr "PREFIJO"

#: pacman_mirrors/constants/txt.py:46
msgid "PROTO"
msgstr "PROTO"

#: pacman_mirrors/constants/txt.py:47
msgid "SECONDS"
msgstr "SEGUNDOS"

#: pacman_mirrors/constants/txt.py:48 pacman_mirrors/constants/txt.py:125
msgid "URL"
msgstr "URL"

#: pacman_mirrors/constants/txt.py:49
msgid "USAGE"
msgstr "USO"

#: pacman_mirrors/constants/txt.py:51
msgid "Return branch from configuration"
msgstr "Devolver a la rama de la configuración"

#: pacman_mirrors/constants/txt.py:52
msgid "Set prefix to"
msgstr "Establecer el prefijo a"

#: pacman_mirrors/constants/txt.py:53
msgid "Replace protocols in configuration"
msgstr "Sustituir los protocolos en la configuración"

#: pacman_mirrors/constants/txt.py:54
msgid "Replace branch in mirrorlist"
msgstr "Sustituir la rama en la lista se réplicas"

#: pacman_mirrors/constants/txt.py:55
msgid "Replace branch in configuration"
msgstr "Sustituir la rama en la configuración"

#: pacman_mirrors/constants/txt.py:56
msgid "Replace mirror url in mirrorlist"
msgstr "Sustituir la url de la réplica en la lista se réplicas"

#: pacman_mirrors/constants/txt.py:57
msgid "Branch name"
msgstr "Nombre de la rama"

#: pacman_mirrors/constants/txt.py:58
msgid "Comma separated list of countries, from which mirrors will be used"
msgstr "Lista de países separados por comas, de loa cuales se usarán las réplicas"

#: pacman_mirrors/constants/txt.py:60
msgid "Load default mirror file"
msgstr "Cargar el archivo de la réplica por defecto"

#: pacman_mirrors/constants/txt.py:61
msgid "Generate mirrorlist with a number of up-to-date mirrors. Ignores:"
msgstr "Generar una lista de réplicas con una serie de réplicas actualizadas. Ignorar:"

#: pacman_mirrors/constants/txt.py:63
msgid "Generate mirrorlist with defaults"
msgstr "Generar una lista de réplicas con los valores por defecto"

#: pacman_mirrors/constants/txt.py:64
msgid "Get current country using geolocation"
msgstr "Obtener el país actual mediante geolocalización"

#: pacman_mirrors/constants/txt.py:65
msgid "List all available countries"
msgstr "Listar todos los países disponibles"

#: pacman_mirrors/constants/txt.py:66
msgid "Generate custom mirrorlist"
msgstr "Generar una lista de réplicas personalizada"

#: pacman_mirrors/constants/txt.py:67
msgid "Generation method"
msgstr "Método de generación"

#: pacman_mirrors/constants/txt.py:68
msgid "Use to skip generation of mirrorlist"
msgstr "Usar para omitir la generación de una lista de réplicas"

#: pacman_mirrors/constants/txt.py:69
msgid "Quiet mode - less verbose output"
msgstr "Modo silencioso - salida menos detallada"

#: pacman_mirrors/constants/txt.py:70
msgid "Syncronize pacman databases"
msgstr "Sincronizar las bases de datos de pacman"

#: pacman_mirrors/constants/txt.py:71
msgid "Maximum waiting time for server response"
msgstr "Tiempo de espera máximo para la respuesta del servidor"

#: pacman_mirrors/constants/txt.py:72
msgid "Print the pacman-mirrors version"
msgstr "Imprimir la versión de pacman-mirrors"

#: pacman_mirrors/constants/txt.py:74
msgid "Branch in config is changed"
msgstr "Se ha cambiado la rama en la configuración"

#: pacman_mirrors/constants/txt.py:75
msgid "Protocols in config is changed"
msgstr "Se han cambiado los protocolos en la configuración"

#: pacman_mirrors/constants/txt.py:76
msgid "Re-branch requires a branch to work with"
msgstr "Cambiar las ramas requiere de una rama para trabajar"

#: pacman_mirrors/constants/txt.py:77
msgid "Branch in mirror list is changed"
msgstr "Se ha cambiado la rama en la lista de réplicas"

#: pacman_mirrors/constants/txt.py:78
msgid "Available countries are"
msgstr "Los países disponibles son"

#: pacman_mirrors/constants/txt.py:79
msgid "Could not download from"
msgstr "No se ha podido descargar desde"

#: pacman_mirrors/constants/txt.py:80
msgid "Cannot read file"
msgstr "No se puede leer el archivo"

#: pacman_mirrors/constants/txt.py:81
msgid "Cannot write file"
msgstr "No se puede escribir el archivo"

#: pacman_mirrors/constants/txt.py:82
msgid "Converting custom mirror file to new format"
msgstr "Convirtiéndo el archivo de réplica personalizado al formato nuevo"

#: pacman_mirrors/constants/txt.py:83
msgid "Custom mirror file"
msgstr "Archivo de réplica personalizado"

#: pacman_mirrors/constants/txt.py:84
msgid "Custom mirror file saved"
msgstr "Archivo de réplica personalizado guardado"

#: pacman_mirrors/constants/txt.py:85
msgid "User generated mirror list"
msgstr "Lista de réplicas generada por el usuario"

#: pacman_mirrors/constants/txt.py:86
msgid "Deprecated argument"
msgstr "Argumento obsoleto"

#: pacman_mirrors/constants/txt.py:87
msgid "doesn't exist."
msgstr "no existe."

#: pacman_mirrors/constants/txt.py:88
msgid "Downloading mirrors from"
msgstr "Descargando réplicas desde"

#: pacman_mirrors/constants/txt.py:89
msgid "Falling back to"
msgstr "Volviendo a"

#: pacman_mirrors/constants/txt.py:90
msgid "Internet connection appears to be down"
msgstr "Parece que no hay conexión a internet"

#: pacman_mirrors/constants/txt.py:91
msgid "Mirror list is generated using random method"
msgstr "La lista de réplicas se genera por un método aleatorio"

#: pacman_mirrors/constants/txt.py:92
msgid "is missing"
msgstr "no se encuentra"

#: pacman_mirrors/constants/txt.py:93
msgid "The mirror file"
msgstr "El archivo de réplica"

#: pacman_mirrors/constants/txt.py:94
msgid "Mirror list generated and saved to"
msgstr "Lista de réplicas generada y guardada en "

#: pacman_mirrors/constants/txt.py:95
msgid "Generated on"
msgstr "Generada en"

#: pacman_mirrors/constants/txt.py:96
msgid "custom mirrorlist"
msgstr "lista de réplicas personalizada"

#: pacman_mirrors/constants/txt.py:97
msgid "to reset custom mirrorlist"
msgstr "para resetear la lista de réplicas personalizada"

#: pacman_mirrors/constants/txt.py:98
msgid "default mirrorlist"
msgstr "lista de réplicas por defecto"

#: pacman_mirrors/constants/txt.py:99
msgid "to modify mirrorlist"
msgstr "para modificar la lista de réplicas"

#: pacman_mirrors/constants/txt.py:100
msgid "Mirror ranking is not available"
msgstr "El ranking de réplicas no está disponible"

#: pacman_mirrors/constants/txt.py:101
msgid "Must have root privileges"
msgstr "Debes tener privilegios de root"

#: pacman_mirrors/constants/txt.py:102
msgid "The mirrors has not changed"
msgstr "Las réplicas no han cambiado"

#: pacman_mirrors/constants/txt.py:103
msgid "No mirrors in selection"
msgstr "No hay réplicas en la selección"

#: pacman_mirrors/constants/txt.py:104
msgid "Option"
msgstr "Opción"

#: pacman_mirrors/constants/txt.py:105
msgid "Please use"
msgstr "Por favor, usar"

#: pacman_mirrors/constants/txt.py:106
msgid "Querying mirrors"
msgstr "Consultando las réplicas"

#: pacman_mirrors/constants/txt.py:107
msgid "Randomizing mirror list"
msgstr "Aleatorizando la lista de réplicas"

#: pacman_mirrors/constants/txt.py:108
msgid "To remove custom config run "
msgstr "Para eliminar la configuración personalizada ejecuta"

#: pacman_mirrors/constants/txt.py:109
msgid "This may take some time"
msgstr "Esto puede tomar algo de tiempo"

#: pacman_mirrors/constants/txt.py:110
msgid "unknown country"
msgstr "país desconocido"

#: pacman_mirrors/constants/txt.py:111
msgid "Use 0 for all mirrors"
msgstr "Usar 0 para todas las réplicas"

#: pacman_mirrors/constants/txt.py:112
msgid "Using all mirrors"
msgstr "Usando todas las réplicas"

#: pacman_mirrors/constants/txt.py:113
msgid "Using custom mirror file"
msgstr "Usando un archivo de réplica personalizado"

#: pacman_mirrors/constants/txt.py:114
msgid "Using default mirror file"
msgstr "Usando un archivo de réplica por defecto"

#: pacman_mirrors/constants/txt.py:115
msgid "Writing mirror list"
msgstr "Escribiendo la lista de réplicas"

#: pacman_mirrors/constants/txt.py:118
msgid "Manjaro mirrors by response time"
msgstr "Réplicas de Manjaro según el tiempo de respuesta"

#: pacman_mirrors/constants/txt.py:119
msgid "Manjaro mirrors in random order"
msgstr "Réplicas de Manjaro en orden aleatorio"

#: pacman_mirrors/constants/txt.py:120
msgid "Check mirrors for your personal list"
msgstr "Comprobar las réplicas para la lista personal"

#: pacman_mirrors/constants/txt.py:121
msgid "Use"
msgstr "Uso"

#: pacman_mirrors/constants/txt.py:122
msgid "Country"
msgstr "País"

#: pacman_mirrors/constants/txt.py:123
msgid "Resp"
msgstr "Tiempo"

#: pacman_mirrors/constants/txt.py:124
msgid "Sync"
msgstr "Sincronizar"

#: pacman_mirrors/constants/txt.py:126
msgid "Cancel"
msgstr "Cancelar"

#: pacman_mirrors/constants/txt.py:127
msgid "OK"
msgstr "De acuerdo"

#: pacman_mirrors/constants/txt.py:128
msgid "Confirm selections"
msgstr "Confirmar las selecciones"

#: pacman_mirrors/constants/txt.py:129
msgid "I want to use these mirrors"
msgstr "Quiero usar estas réplicas"
